
/*============================
=            Init            =
============================*/

var app = angular.module('myApp', ['ngRoute']);

// app.config(function($routeProvider){
// 		$routeProvider
// 			.when("/checkTodo", { redirectTo: "/test.html"})
// 			.otherwise({redirectTo: "/"})
// })

var ip = window.localStorage.getItem('ip');
window.localStorage.setItem('todo', "");

/*================================================
=            Directives personalisées            =
================================================*/



app.directive("ngBlur", function(){
	return function(scope, elem, attr){
		elem.bind("blur", function(){
			scope.$apply(attr.ngBlur)
		})
	}
})

/*===================================
=            Application            =
===================================*/

app.controller("TodoController", function($scope, filterFilter, $http, $location){

	/*=================================================
	=            Declaration des variables            =
	=================================================*/

	$scope.todos = [];
	$scope.placeholer = "Chargement...";
	$scope.statusFilter = "";
	$scope.editing = false;
	/*=================================
	=            Functions            =
	=================================*/

	/*----------  Récupérer todos  ----------*/
	
	
	var interval = setInterval(function(){
		getTodo()
	}, 1000)

	$scope.stopInterval = function () {
		console.log("stop stop")
		clearInterval(interval)
	}

	/*----------  Supprimer une todo  ----------*/
	
 	$scope.removeTodo = function (index, todo){
 		$scope.todos.splice(index, 1);

		$http.put(
				"http://" + ip + "/scripts/deleteTodo.php",
				{
					id: todo.id
				},
				{'Content-Type': 'application/x-www-form-urlencoded'
		}).then(function(data){
			// console.log(data)
		}, function(resp){
			// console.log(resp)
		});
 	}

 	/*----------  Créer une todo  ----------*/
 	
 	$scope.addTodo = function(){

 		// Ajoute le todo
		$http.post("http://" + ip + "/scripts/createTodo.php",{name: $scope.newtodo},{'Content-Type': 'application/x-www-form-urlencoded'
		}).then(function(data){
			// console.log(data)
		}, function(resp){
			// console.log(resp)
		});
         
        setCache()
        
		$http.get("http://" + ip + "/scripts/getTodo.php").success(function(data){
			$scope.todos = data
			$scope.placeholder = "Ajouter une note...";
			console.log(data)
		}).error(function(){
			console.log("erreur chargement des todo")
		})

		$scope.newtodo = "";
	}
	function setCache(){
//        console.log(window.localStorage.getItem('todo'))
        todos = window.localStorage.getItem('todo');
        
        todoSplit = todos.split("&&")
        if (todoSplit.length >= 5){
            console.log("complet")
            todoSplit[0] = todoSplit[1];
            todoSplit[1] = todoSplit[2];
            todoSplit[2] = todoSplit[3];
            todoSplit[3] = todoSplit[4];
            todoSplit[4] = todoSplit[5];
            todoSplit[5] = $scope.newTodo
        }
        console.log(todoSplit)
        
        window.localStorage.setItem('todo', todos + "&&" + $scope.newtodo );
//        console.log(window.localStorage.getItem("todo"))
    }
    
    function getCache(){
        todos = window.localStorage.getItem("todo");
        console.log(todos)
    }
    
	/*----------  Update Todo  ----------*/

 	$scope.editTodo = function (todo) {
 		

 		if (todo.name == "") {
 			todo.editing = false;
 			return;
 		}

		$http.put(
				"http://" + ip + "/scripts/updateTodo.php",
				{
					name: todo.name, 
					id: todo.id
				},
				{'Content-Type': 'application/x-www-form-urlencoded'
		}).then(function(data){
			// console.log(data)

			interval = setInterval(function(){
				getTodo()
			}, 1000)
			
		}, function(resp){
			// console.log(resp)
		});

 		todo.editing = false;

 	}

 	/*----------  Check Todo  ----------*/
 	
 	$scope.checkTodo = function(todo) {
 		if (todo.completed == 0){
			todo.completed = 1
 		} else if (todo.completed == 1){
			todo.completed = 0
 		}


		$http.put(
				"http://" + ip + "/scripts/checkTodo.php",
				{
					name: todo.name, 
					id: todo.id,
					completed: todo.completed
				},
				{'Content-Type': 'application/x-www-form-urlencoded'
		}).then(function(data){
			// console.log(data)
		}, function(resp){
			// console.log(resp)
		});
 	}

 	/*----------  Filtre footer  ----------*/
 
 	if ($location.path() == "") { $location.path("/")}
 	$scope.location = $location;
	$scope.$watch("location.path()", function(path){

		if (path == "/active") {
			$scope.statusFilter = {completed : "0"}
		} else if (path == "/done"){
			$scope.statusFilter = {completed : "1"}
		} else {
			$scope.statusFilter = ""
		}

	})
	
	/*----------  Récupère la liste des todos  ----------*/

	function getTodo () {
		if ($scope.editing == true){
			clearInterval(interval)
		}
		$http.get("http://" + ip + "/scripts/getTodo.php").success(function(data){
			$scope.todos = data
			$scope.placeholder = "Ajouter...";
			 console.log("chargement ok")
             
		}).error(function(){
			 console.log("erreur chargement des todo")
             
		})
	}

})



