<?php 
	require("DBConn.php");

	class Controller extends DBConn{
		function __construct(){
           	parent::__construct();
		}


	    public function getTodo()
	    {
	        $sql = "SELECT * FROM posts 
	        		WHERE deleted = '' 
	        		OR deleted = 'false'";
	   
	        $stmt = $this -> dbh -> query($sql);
	        $result = $stmt -> fetchAll(PDO::FETCH_ASSOC);
	
	        $result = json_encode($result);
        	
        	echo $result;
	    } 

	    public function createTodo($data){
	    	$sql = "INSERT INTO `posts` (`id`, `user_id`, `name`, `completed`) 
	    			VALUES (NULL, '1', :name, 'false')";

	        $stmt = $this->dbh->prepare($sql);

	        $params = ['name' => $data->name];
	        $stmt->execute($params);

	    }

	    public function updateTodo($data){
	    	$sql = "UPDATE posts 
	    			SET name = :new_name 
	    			WHERE posts.id = :id";
       		
       		$stmt = $this->dbh->prepare($sql);

	        $params = [
	        			'new_name' => $data->name,
	        			'id' => $data->id
	        			];
	        $stmt->execute($params);
	    }

	    public function deleteTodo($data){
	    	$sql = "UPDATE posts 
	    			SET deleted = :deleted
	    			WHERE posts.id = :id";
       		
       		$stmt = $this->dbh->prepare($sql);

	        $params = [
	        			'deleted' => "true",
	        			'id' => $data->id
	        			];
	        $stmt->execute($params);
	    }

	    public function checkTodo($data){

	    	$sql = "UPDATE posts 
	    			SET completed = :completed
	    			WHERE posts.id = :id";
       		
       		$stmt = $this->dbh->prepare($sql);
	        $params = [
	        			'completed' => $data->completed,
	        			'id' => $data->id
	        			];

	        $stmt->execute($params);

	    }

	}
?>